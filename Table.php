<?php
/**
 * Copyright 2013 Jordan Sterling
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Jordan's awesome "ORM layer"
 */
abstract class Table
{
	/**
	 * Table name
	 * @var String
	 */
	static protected $table;

	/**
	 * The sub classes name
	 * @var String
	 */
	static protected $class;

	/**
	 * A string list of column names
	 * @var array
	 */
	static protected $columns = array();

	/**
	 * A mapping array. The keys are alias names, the values are the real
	 * column names
	 * @var array
	 */
	static protected $column_aliases = array();

	/**
	 * Default parameters passed in to every query. This array just has the
	 * column name, when a query goes through that column will be added.
	 * @var array
	 */
	static protected $implicit_parameters = array();

	/**
	 * If you are using implicit parameters set this to true
	 * @var boolean
	 */
	static protected $has_implicit_parameters = false;

	/**
	 * Cached DB reference
	 * @var DB
	 */
	static public $db = null;

	/**
	 * The database query initially populates this array in init(). It contains
	 * all the values that would be in the row of the table.
	 * @var array
	 */
	private $_values = array();

	/**
	 * Array of booleans indicating if any row was modified since read
	 * @var array
	 */
	private $_changed = array();

	/**
	 * Abstract models let you share a single physical table with multiple subclasses
	 * that all represent rows of that table. If you are using this functionality
	 * the parent model needs to set abstract to true. You also must override the
	 * subclass_map() function to define how the abstract columns map to impl.
	 *
	 */
	static protected $abstract = false;

	/** remember to parent() */
	public function __construct()
	{
		static::$has_implicit_parameters = count(static::$implicit_parameters) > 0;
	}

	/**
	 * handles getters for the columns
	 */
	public function __call($function, $args)
	{
		$function = static::de_alias($function);
		if (in_array($function, static::$columns))
		{
			if (isset($this->_values[$function]))
				return $this->_values[$function];
			else
				return null;
		}
		throw new \Exception("Method $function does not exist in class " . get_called_class());
	}

	/**
	 * Search for a row with an array
	 *
	 * Parameters:
	 * $data {Array} The keys are the column names, the values are the cell values.
	 * data will have to be an array with keys:
	 * and => array of WHERE cases to AND by( column => value )
	 * or => array of WHERE cases to OR by  ( column => value)
	 * in => array of items to look for.    ( column => array of values)
	 *
	 * As a legacy feature, if none of the 3 supported types are
	 * present it will default as if it were an and array
	 *
	 * $limit {int} How many to return. Defaults to 1.
	 * TODO: Make limit a param like those above
	 *
	 * Returns
	 * {User} A single user object if and only if $limit == 1
	 *
	 * OR
	 *
	 * {Array} Array of user objects if $limit != 1
	 */
	public static function find($data, $limit = 1)
	{
		$query = 'SELECT * FROM ' . static::$table;
		$values = array();
		if (count($data) > 0)
		{

			$query .= ' WHERE ';
			$special_keys = array('and', 'or', 'in', 'order', 'where');
			$keys_represent_query_parts = count(array_intersect($special_keys, array_keys($data))) > 0;
			if ($keys_represent_query_parts)
			{
				if (isset($data['and']))
					foreach (static::$implicit_parameters as $implicit_key => $implicit_value)
						$data['and'][$implicit_key] = $implicit_value;
				else if (static::$has_implicit_parameters)
					$data['and'] = static::$implicit_parameters;
				foreach ($data as $clause_type => $params)
				{
					switch ($clause_type)
					{
					case 'and':
						foreach ($params as $column => $val)
						{
							$column = addslashes(static::de_alias($column));
							$query .= "`$column` = ? AND ";
							$values[] = $val;
						}
						$query = substr($query, 0, -4); // Remove the trailing "AND "
						break;
					case 'or':
						foreach ($params as $column => $val)
						{
							$column = addslashes(static::de_alias($column));
							$query .= "`$column` = ? OR ";
							$values[] = $val;
						}
						$query = substr($query, 0, -3); // Remove the trailing "OR "
						break;
					case 'in':
						foreach ($params as $column => $val)
						{
							if (count($val) === 0)
								continue;
							$column = addslashes(static::de_alias($column));
							if (count($values) > 0)
							{
								$query .= " AND ";
							}
							$query .= "`$column` IN ( ";
							foreach ($val as $v)
							{
								$query .= "?, ";
								$values[] = $v;
							}
							$query = substr($query, 0, -2); // Remove the trailing ", "
							$query .= ') ';
						}
						break;
					case 'where':
						// params is a string of query
						$query .= " $params ";
						break;
					case 'values':
						// params is an array of values in order used in the where clause
						foreach ($params as $k => $v)
							$values[$k] = $v;
						break;
					}
				}
			}
			else // just an array, no key
			{
				$ip = static::$implicit_parameters;
				foreach ($data as $key => $value)
					$ip[$key] = $value;
				$data = $ip;
					
				$values = array();
				foreach ($data as $column => $val)
				{
					$column = addslashes(static::de_alias($column));
					$query .= "`$column` = ? AND ";
					$values[] = $val;
				}
				$query = substr($query, 0, -4); // Remove the trailing "AND "
			}
		}
		if (count($values) === 0)
		{
			$query = substr($query, 0, -6); // Remove the trailing "WHERE "
		}
		if (array_key_exists('order', $data))
		{
			$query .= ' ORDER BY ' . static::de_alias($data['order']);
		}
		// If limit is not an int, assume we want all results.
		if (is_int($limit))
		{
			$query .= ' LIMIT ?';
			$values[] = $limit;
		}
		if (Table::$db === null)
			Table::get_db();
		
		$results = Table::$db->query($query, $values);
		if ($results === false || count($results) < 1)
			return false;
		$items = array();
		foreach ($results as $row)
		{
			if (static::$abstract)
				$class_name = static::subclass_map($row);
			else
				$class_name = static::$class;

			$clss = "\\model\\$class_name";
			$item = new $clss;
			$item->init($row);
			$items[] = $item;
		}
		// convenience so if you dont specify a limit and it defaults to 1
		// you dont get an array back
		if ($limit === 1)
			$items = $items[0];
		return $items;
	}

	/**
	 * you will need to modify this in your own code to provide the db
	 */
	protected static function get_db()
	{
		Table::$db = new DB('host', 'user', 'pass', 'db');
	}
	/**
	 * An alias for find when you want a query with no limit on result size.
	 * This uses the trick that $limit must be an integer, or it is ignored.
	 * If you give it null, it will ignore limit.
	 *
	 * See User::find for documentation on the $data parameter and the return
	 * value.
	 */
	public static function find_all($data = array())
	{
		$ret = self::find($data, null);
		if ($ret === false)
			return array();
		return $ret;
	}

	/**
	 * gets all rows from table. dont use on large tables.
	 */
	public static function all()
	{
		if (Table::$db === null)
			Table::get_db();
		$query = 'SELECT * FROM ' . static::$table;
		$results = Table::$db->query($query);
		$items = array();
		foreach ($results as $row)
		{
			$item = new static::$class();
			$item->init($row);
			$items[] = $item;
		}
		return $items;
	}

	/**
	 * Helper function when you have an array of row objects.
	 * Lets say you have a list of rows, but want an array of thier row ids.
	 * Then you would flatten the list and pass 'id' as the key.
	 * This would give back an array of only the ids
	 *
	 * Parameters:
	 * $list_of_rows - An array of User objects
	 * $key_you_want - One of the keys of that array
	 *
	 * Returns:
	 * {Array} Containing all the values of that key in the row list
	 */
	public static function flatten($list_of_rows, $key_you_want)
	{
		$keepers = array();
		foreach ($list_of_rows as $row)
			$keepers[] = $row->$key_you_want();
		return $keepers;
	}
	/**
	 * Deletes the current item
	 *
	 * Returns
	 * {Boolean} true on success, false otherwise
	 */
	public function delete()
	{
		// Basic precaution to not delete the entire table
		$id = $this->id();
		if (!is_int($id) || (is_int($id) && $id < 0))
			return false;
		
		if (Table::$db === null)
			Table::get_db();
		$query = 'DELETE FROM ' . static::$table . ' WHERE `id` = ? LIMIT 1';
		$results = Table::$db->query($query, array($id));
		return $results !== false;
	}

	public function init($data)
	{
		foreach ($data as $name => $value)
		{
			if (isset(static::$column_aliases[$name]))
			{
				unset($data[$name]);
				$data[static::de_alias($name)] = $value;
			}
		}
		foreach (static::$implicit_parameters as $name => $val)
			$data[$name] = $val;

		$this->_values = $data;
		foreach (static::$columns as $col)
			$this->_changed[$col] = false;
	}

	public function set($column, $value)
	{
		$column = static::de_alias($column);
		$this->_changed[$column] = true;
		$this->_values[$column] = $value;
	}

	public static function get_columns()
	{
		return static::$columns;
	}


	/**
	 * Saves a single column to the DB! This does a write!
	 * Returns
	 * {Boolean} true on success, false otherwise
	 */
	public function write($column, $value)
	{
		// Basic precaution to not update over the entire user table
		$id = intval($this->id());
		$column = addslashes(static::de_alias($column));
		$query = "UPDATE " . static::$table . " SET `$column` = ? WHERE `id` = ? LIMIT 1";
		
		if (Table::$db === null)
			Table::get_db();
		$results = Table::$db->query($query, array($value, $this->id()));
		$this->_values[$column] = $value;
		$this->_changed[$column] = false;
		return $results !== false;
	}
	// saves this class
	// if id is ggreater than zero, this is considered old and does an update on the id
	// otherwise this is considered new and does an insert
	public function save()
	{
		if ($this->id() > 0)
			$this->update();
		else
			$this->insert();
	}

	private function update()
	{
		$query = 'UPDATE ' . static::$table . ' SET ';
		$params = array();
		foreach (static::$columns as $col)
		{
			if (!isset($this->_changed[$col]) || !$this->_changed[$col])
				continue;
			$this->_changed[$col] = false;
			$col = addslashes($col);
			$query .= "`$col` = ?, ";
			$params[] = $this->$col();
		}
		if (count($params) === 0)
			return; // nothing changed, don't execute update
		$query = substr($query, 0, -2); // extra ', ' trailing on the query
		$query .= ' WHERE `id` = ? LIMIT 1';
		$params[] = $this->id();
		if (Table::$db === null)
			Table::get_db();
		return Table::$db->query($query, $params);
	}

	private function insert()
	{
		$query = 'INSERT INTO ' . static::$table . ' (';
		$values = '(';
		$params = array();
		foreach (static::$columns as $col)
		{
			if ($col == 'id' || $this->$col() === NULL)
				continue;
			$this->_changed[$col] = false;
			$col = addslashes($col);
			$query .= "`$col`, ";
			$values .= '?, ';
			$params[] = $this->$col();
		}
		$query = substr($query, 0, -2);
		$values = substr($values, 0, -2);
		$values .= ') ';
		$query .= ') VALUES ' . $values;
		if (Table::$db === null)
			Table::get_db();
		$id = Table::$db->query($query, $params);
		if ($id)
			$this->_values['id'] = $id;
		return $id;
	}

	private static function de_alias($column_name)
	{
		if (isset(static::$column_aliases[$column_name]))
			return static::$column_aliases[$column_name];
		return $column_name;
	}

	// needs to be overridden in subclass
	public function to_array()
	{
		return array();
	}
}
